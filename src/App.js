import React, { Component } from "react";
import TodoInput from "./AppComponents/TodoInput";
import TodoList from "./AppComponents/TodoList";
import uuid from "uuid";
class App extends Component {
state={
  items:[],
  id:uuid(),
  item:'',
  editItem:false
};
handleChange = e =>{
  this.setState({
    item: e.target.value
  });
}
handleSubmit = e =>{
e.preventDefault();

const newItem ={
  id:this.state.id,
  title:this.state.item
};
console.log(newItem);

const updateItems = [...this.state.items, newItem];

this.setState({
  items: updateItems,
  item:"",
  id: uuid(),
  editItem: false
});
};
clearList = () =>{
  this.setState({
    items:[]
  });
};
handleDelete = (id) =>{
  const filterdItems = this.state.items.filter(item =>item.id !== id)
  this.setState({
    items:filterdItems
  });
};
handleEdit = id =>{
  const filterdItems = this.state.items.filter(item =>item.id !== id)

const selectedItem = this.state.items.find(item => item.id ===id);
console.log(selectedItem);
  this.setState({
    items: filterdItems,
    item: selectedItem.title,
    editItem:true,
    id:id

  });
};
render() {
  return (
    <div className="container">
      <div className="col-10 mx-auto col-md-8 mt-4">
        <h3 class="">Todo Input</h3>
        <TodoInput item={this.state.item} handleChange={this.handleChange}
          handleSubmit={this.handleSubmit}
          editItem={this.state.editItem}/>
        <TodoList items={this.state.items} clearList={this.clearList}
          handleDelete={this.handleDelete}handleEdit={this.handleEdit}/>
      </div>
    </div>

  );
}
}
export default App;
