import React from 'react';
import List_style from '../components/List_style.css';
function Items(props){
  const items = props.items;
  const listItems = items.map(item =>{
    return <div className="list" key={"item.key"}>
     <p>
        <input type="text"
        id={item.key}
        value={item.text}
        onChange ={
          (e) =>{
            props.setUpdate(e.target.value, item.key)
          }
        }/>
        <span className="delete"
        onClick={ () => props.deleteItem(item.key)}>DELETE </span>
     </p>
     </div>
  }
  )
  return(
    <div>{listItems}</div>
  )
}

export default Items;
