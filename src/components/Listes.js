import React from 'react';
import Items from "../components/Items";
import ReactDOM from 'react-dom';
class Listes extends React.Component {
  constructor(props){
    super(props);
    this.state={
      items:[],
      currentItems:{
        text:'',
        key:''
      }
    }
    this.handleInput = this.handleInput.bind(this);
    this.addItem = this.addItem.bind(this);
    this.deleteItem = this.deleteItem.bind(this);
    this.setUpdate = this.setUpdate.bind(this);
  }
  handleInput(e){
    this.setState({
      currentItems:{
        text: e.target.value,
        key:Date.now()
      }
    })
  }
  addItem (e){
    e.preventDefault();
    const newItem = this.state.currentItems;
    console.log(newItem);
    if(newItem.text!== ""){
      const newItems=[...this.state.items,newItem];
      this.setState({
        items:newItems,
        currentItems:{
          text:'',
          key:''
        }
      })
    }
  }
  deleteItem(key){
    const filterItems = this.state.items.filter(item =>
    item.key!==key);
    this.setState({
      items:filterItems
    })
  }
  setUpdate(text, key){
    const items = this.state.items;
    items.map(item =>{
      if(item.key===key){
        item.text=text;
      }
    })
    this.setState({
      items: items
    })
  }
   render(){
     return(
      <div className="list">
         <header>
            <form id="to-do-form"onSubmit={this.addItem}>
                <input type="text" placeholder="enter text"
                value={this.state.currentItems.text}
                onChange={this.handleInput}/>
                <button type="submit">Add </button>
            </form>
          </header>
          <Items items = {this.state.items}
          deleteItem = {this.deleteItem}
          setUpdate = {this.setUpdate}></Items>
        </div>

     );
   }

  }

export default Listes;
